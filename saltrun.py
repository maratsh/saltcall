__author__ = 'maratsh'

import json
import sys
import os
from saltclient import request_salt
import argparse

if __name__ == "__main__":

    password = os.environ.get('SALT_PASSWORD')
    username = os.environ.get('SALT_USERNAME')

    parser = argparse.ArgumentParser()

    parser.add_argument('--tgt', dest='targets', help="targets", type=str)
    parser.add_argument('--exp', dest='exp', help="expression form of target string",  choices=['compound'])
    parser.add_argument('--fun', dest='function', help="function o run", type=str)
    parser.add_argument('--arg', dest='argument', action='append', type=str)
    parser.add_argument('--api_host', dest='api_host', type=str)
    parser.add_argument('--client', dest='client', choices=['local', 'local_async', 'wheel', 'runner'], type=str)
    parser.add_argument('--match', dest='match', type=str)

    parser.set_defaults(exp='compound')

    opt = parser.parse_args()

    q = {"client": opt.client,
       "tgt": opt.targets,
       "expr_form": opt.exp,
       "fun": opt.function, "arg": opt.argument, "match": opt.match}

    print "Call job sync: ", json.dumps(q, indent=4, sort_keys=True)

    jobs = request_salt(q,opt.api_host, username, password)
    print json.dumps(jobs, indent=4, sort_keys=True)

    sys.exit(0)