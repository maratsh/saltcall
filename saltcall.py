import json
import time
import sys
import os
from saltclient import request_salt
import argparse


def get_job_id(jobs):
    for job in jobs:
        try:
            jid = job[u'jid']
            minions = job[u'minions']
        except (KeyError,TypeError):
            raise Exception("cannot get job id or minion id from salt-api:" + str(jobs))

    return jid, minions


def isdone(minions, jid):
    done = 0
    try:
        for minion_id in minions:
            q = {"client": "local",
                 "tgt": minion_id,
                 "fun": "saltutil.find_job",
                 "arg": [jid]}
            status = request_salt(q, api_host, username, password)
            for task in status:
                if "jid" in task[minion_id]:
                    print jid, minion_id, "task running"
                else:
                    print jid, minion_id, "task done"
                    done += 1
    except (KeyError, AttributeError):
        print "Some errors in salt-api output:", status
    return done


def checkout(s):
    success = 0
    fail = 0

    if 'data' in s and '__id__' not in s['data']:
        data = s[u'data']
    else:
        data = s
    for minion_id, output in data.iteritems():
        try:
            for states, values in output.iteritems():

                if values[u'result'] == True:
                    success = success + values[u'result']
                else:
                    fail = fail + (not values[u'result'])
        except (KeyError, AttributeError):
            print "Some errors in salt-api output:", status
            fail = fail + 1
    return success, fail


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('--tgt', dest='targets', help="targets", type=str)
    parser.add_argument('--exp', dest='exp', help="expression form of target string",  choices=['compound'])
    parser.add_argument('--fun', dest='function', help="function o run", type=str)
    parser.add_argument('--arg', dest='argument', action='append', type=str)
    parser.add_argument('--kwarg', dest='kwargument', action='store', type=str)
    parser.add_argument('--api_host', dest='api_host', type=str)
    parser.add_argument('--match', dest='match', type=str)

    parser.set_defaults(exp='compound')
    parser.set_defaults(kwargument=str('{ "foo": "bar" }'))
    #example --kwarg='{"pillar": { "version": "1.2016.05.04-b1" }}'

    opt = parser.parse_args()

    api_host = opt.api_host
    password = os.environ.get('SALT_PASSWORD')
    username = os.environ.get('SALT_USERNAME')
    success = False
    fail = False
    status = []

    value = False
    q = {"client": "local_async",
         "tgt": opt.targets,
         "expr_form": opt.exp,
         "fun": opt.function, "arg": opt.argument,
         "kwarg": json.loads(opt.kwargument) }


    print "Call job async: ", json.dumps(q, indent=4, sort_keys=True)
    jobs = request_salt(q, api_host, username, password)
    jid, minions = get_job_id(jobs)

    print "Start lookup job: ", jid, "for minions:", minions
    while not isdone(minions, jid) == len(minions):
        time.sleep(1)

    q = {"client": "runner",
         "fun": "jobs.lookup_jid",
         "jid": jid}
    status = request_salt(q, api_host, username, password)
    for s in status:
        if len(s) > 0:
            success, fail = checkout(s)

    print "Job", jid, "done: ", json.dumps(status, indent=4, sort_keys=True)
    print "States OK:", success, "States NOT OK:", fail

    if fail:
        sys.exit(1)
    elif success:
        sys.exit(0)
    else:
        sys.exit(1)
