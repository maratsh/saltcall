__author__ = 'maratsh'

import httplib2
import json


def request_salt(request, api_host, username, password):
    request["username"] = username
    request["password"] = password
    request["eauth"] = "pam"
    headers = {"Content-Type": "application/json"}
    body = json.dumps([request])
    h = httplib2.Http(".cache")
    resp, content = h.request(api_host, "POST", headers=headers, body=body)
    if resp['status'] == '500':
        raise Exception("salt-master not answers to salt-api ",
                        "response status = " + str(resp),
                        "response content = " + str(content))
    if resp['status'] != '200' and resp['status'] != '204':
        raise Exception("response status = " + str(resp), "response content = " + str(content))
    result = json.loads(content)
    return result[u'return']
